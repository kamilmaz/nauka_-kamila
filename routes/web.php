<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});

Route::get('/contact', 'PagesController@contact');
Route::get('/about', 'PagesController@about');
Route::get('/videosdes/{id}','VideosController@dest');
Route::get('/category','VideosController@category');
Route::post('/category','VideosController@categoryadd');
Route::get('/upload','VideosController@showu');
Route::post('/upload/image','VideosController@stores');
//zapis daje możliwość walidacji
Route::group(['middleware' => ['web']], function ()


			{
			//Route::get('/videos', 'VideosController@index');
			//Route::post('/videos', 'VideosController@store');
			//Route::get('/videos/create', 'VideosController@create');
			//Route::get('/videos/{id}', 'VideosController@show');
				Route::resource('videos','VideosController');
			}

	);
Auth::routes();
//Route::post('/destroy/{id}','VideosController@destroy');
Route::get('/home', 'HomeController@index')->name('videos.index');
