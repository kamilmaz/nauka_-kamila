@extends('master')
@section('content')

<div class="videos-header card">
    <h2>Najnowsze filmy</h2>
</div>
<div class="row">
  @foreach($videos1 as $vid)
    <!-- Single video -->
		    <div class="col-xs-12 col-md-6 col-lg-4 single-video">
		        <div class="card">
		        
		            <div class="embed-responsive embed-responsive-16by9">
		                <iframe class="embed-responsive-item" src="{{ $vid->url }}?showinfo=0" frameborder="0" allowfullscreen></iframe>
		            </div>
		            <div class="card-content">
		           
		            

		                       <a href="{{ action('VideosController@show', $vid->id)}}" "class="btn btn-primary btn-lg">
		                    <h4>{{$vid->title}}</h4>
		                </a>
		           
		                <p>{{$vid->description}}</p>
		                <span class="upper-label">Dodał</span>
		                <span class="video-author">{{ $vid->user->name }}</span>
		                
		                 <span class="upper-label">Kategoria</span>
		                @foreach ($vid->categories as $category)
		                
		                 <span class="video-author">{{$category->name}}</span>
		                @endforeach

		                  <img src='{{ asset('avatars')."/" .$vid->user_photo }}'>

		            </div>
		            
		        </div>
		    </div>

 @endforeach
</div>

@stop