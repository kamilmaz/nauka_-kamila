@extends('master')
@section('content')

<div class="col-xs-12 videos-header card">
    <h2>{{$video->title}}</h2>
</div>

<div class="row">

    <!-- left col. -->
    <div class="col-xs-12 col-md-9 single-video-left">

        <div class="card">

            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="{{$video->url}}?showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
        
            <div class="single-video-content">
                <div class="categories">
                    <h4>Kategorie</h4>
                    @foreach ($video->categories as $category)
                    <a href="{{ $category->name }}">{{ $category->name }}</a>
                    @endforeach 

                </div>
                <h4>Pełny opis</h4>
                <p>{{$video->description}}</p>
                <span class="upper-label">Dodał</span>
                <span class="video-author">Strefa Kursów</span>

                
  <div class="edit-button">
                    <a href="{{ action('VideosController@edit', $video->id)}}" "class="btn btn-primary btn-lg">
                        Edytuj Video
                    </a>
<div class="edit-button">
                    <a href="{{ action('VideosController@dest', $video->id)}}"  class="btn btn-primary btn-lg">
                        Kasuj Video
                    </a>

                </div>
                             <form action="{{ action('VideosController@dest', $video->id)}}" method="get" >
   
  

  <div class="form-group">
  <button class="btn btn-primary">Kasuj</button>
  </div>
</form>
            </div>

@stop
