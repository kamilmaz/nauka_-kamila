@extends('master')
@section('content')
@//var_dump($errors)

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">
            <!-- Formularz -->

     		    @include('videos.form_errors')

            	{!! Form::model($video, ['method'=>'PATCH','class'=>'form-horizontal','action'=>['VideosController@update', $video->id]]) !!}
            		
            	@include('videos.form',['buttonText'=>'Edycja Video'])

            	{!! Form::close() !!}
               

                  <form action="{{ action('VideosController@destroy', $video->id)}}" method="post" >
      <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  

  <div class="form-group">
  <button class="btn btn-primary">Kasuj</button>
  </div>
</form>



            </div>
        </div>
    </div>
</div>
@stop 