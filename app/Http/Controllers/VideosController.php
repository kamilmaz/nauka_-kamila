<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Request;
use App\Video;
use App\Category;
use App\Http\Requests\CreateVideoRequest;
use App\Http\Requests\CreateCategoryRequest;
use Auth;


class VideosController extends Controller
{
    //Metoda do pobierania listy metoda index
       public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() 
    {//turn Auth::user();
    	# code...$zmienna do pokazania, obiekt Video, 



    	$zmienna = Video::latest()->get();
    	return view('videos.index')->with('videos1', $zmienna);
    	//            nazwa katalogu nowy views 

    }

    //jeden film
    public function show($id)

    {
    	$video = Video::findORfail($id);
    	return view('videos.show')->with('video', $video);
    }

     public function create()

    {	//wyswietla formularz dodawania filmu
    	$categories = Category::pluck('name','id');
       
    	return view('videos.create')->with('categories', $categories);
    }

      public function store(CreateVideoRequest $request)

    {	//zapisuje do film do bazy 
    	//$input = Request::all();
    	//Video::create($input);
    	//Video::create($pytanie->all());

        //Auth::user() - pokazuje wszystkie informacjie o zalogowanym user

       // dd($request->CategoryList);
          $this->validate($request, [
          'user_photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

          if ($request->hasFile('user_photo')) {
        //88888888888888888888888
         // get current time and append the upload file extension to it,
        // then put that name to $photoName variable.
        $photoName = time().'.'.$request->user_photo->getClientOriginalExtension();

        /*
        talk the select file and move it public directory and make avatars
        folder if doesn't exsit then give it that unique name.
        */
        $request->user_photo->move(public_path('avatars'), $photoName);
        //return view('videos.upload',compact('user_photo','photoName'));
        //return $photoName;

        //That’s it. Now you can save the $photoName to the database as a user_photo field value. //You can use asset(‘avatars’) function in your view and access the photos.
        //888888888888888888888888

      

        $video = new Video($request->all());
        //do rekordu user_photo wkładam nazwe $photo name 
        $video->user_photo = $photoName;
        Auth::user()->videos()->save($video);

        $categoryIds = $request->input('CategoryList');

        $video->categories()->attach($categoryIds);
        //Session::flash('video_created','Twój nowy film został zapisany');
         }
        
       
        //$user->videos()->first()->name;redirect('videos');

        //$kamil = App\User::where('name', 'Kamil')->first()->videos()->count();


        /// Jako POMOC!!!!!!!!!!
         //$article->title = $request->get('title');
        //article->category_id = $request->get('category_id');
        // $article->image = str_slug($request->get('image'));
         //article->subtitle = $request->get('subtitle');
         //article->description = $request->get('description');

     return redirect('videos');
    }

        public function edit($id)

    {	//formulacz edycji wideo
        $categories = Category::pluck('name','id');
    	$video = Video::findORfail($id);
    	return view('videos.edit',compact('video','categories'));
    }

         public function update($id, CreateVideoRequest $pytanie)
    {	//formulacz edycji wideo
    	$video = Video::findORfail($id);
    	$video->update($pytanie->all());
           $video->categories()->sync($pytanie->input('CategoryList'));
    	return redirect('videos');
    }


    //usuwanie obiktów wideo
            public function dest($id)
    {   //formulacz edycji wideo
       //$video = Video::findORfail($id);
        //$video->delete($pytanie->all());
        $video = Video::find($id);
        $video->delete();
        return redirect('videos');

    }


        public function category()
             //wyświetlanie kategorii

          
    {        
       
        return view('videos.category');

    }
        public function categoryadd(CreateCategoryRequest $request)
             //zapisywanie kategorii

          
    {        
        //$category = new Category($request->all());
        //$category->save();
        Category::create($request->all());
        return redirect('videos');

    }


    public function showu()
      {


        return view('videos.upload');
      }


    public function stores(Request $request)
      {

        // get current time and append the upload file extension to it,
        // then put that name to $photoName variable.
        $photoName = time().'.'.$request->user_photo->getClientOriginalExtension();

        /*
        talk the select file and move it public directory and make avatars
        folder if doesn't exsit then give it that unique name.
        */
        $request->user_photo->move(public_path('avatars'), $photoName);
        //return view('videos.upload',compact('user_photo','photoName'));
        return $photoName;

        //That’s it. Now you can save the $photoName to the database as a user_photo field value. //You can use asset(‘avatars’) function in your view and access the photos.

      }

    
    
}
