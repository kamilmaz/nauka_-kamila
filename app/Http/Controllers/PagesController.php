<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function contact()
    {
    	$header = 'To jest nagłowek strony kontakt';
    	$date = '12/12/2018';
    	$visited = 65456;
    	return view('pages.contact', compact('header', 'date', 'visited'));
    }
    public function about()
    {
    	return view('pages.about');
    }
}
