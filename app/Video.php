<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
    	'id',
    	'user_id',
    	'title',
    	'url',
    	'description'
    ];

    public function user()
    {   //jaki jest autor danego filmu
    	return $this->belongsTo('App\User');
    }
    // film ma wiele kategori
     public function categories()
    {
    return $this->belongsToMany('App\Category');
    }


    //Pokazuje id dla jednego filmu, potrzebe do edycji wideo
    public function getCategoryListAttribute()
    {
        return $this->categories->pluck('id');
    }
}
