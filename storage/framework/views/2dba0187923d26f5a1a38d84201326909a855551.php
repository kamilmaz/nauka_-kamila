<?php $__env->startSection('content'); ?>


<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">
            <!-- Formularz -->
                <div class="form-group">
          	   
                <?php echo e(Form::open(['url'=>'/upload/image', 'files' => true])); ?>

                <?php echo e(Form::label('user_photo', 'User Photo',['class' => 'control-label'])); ?>

                <?php echo e(Form::file('user_photo')); ?>

                <?php echo e(Form::submit('Save', ['class' => 'btn btn-success'])); ?>

                <?php echo e(Form::close()); ?>


                <img src='<?php echo e($c = asset('avatars')); ?>'>
               
                
                
             
                
                
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>