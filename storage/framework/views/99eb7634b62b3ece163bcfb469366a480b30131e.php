<?php $__env->startSection('content'); ?>

<div class="col-xs-12 videos-header card">
    <h2><?php echo e($video->title); ?></h2>
</div>

<div class="row">

    <!-- left col. -->
    <div class="col-xs-12 col-md-9 single-video-left">

        <div class="card">

            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="<?php echo e($video->url); ?>?showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
        
            <div class="single-video-content">
                <div class="categories">
                    <h4>Kategorie</h4>
                    <?php $__currentLoopData = $video->categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e($category->name); ?>"><?php echo e($category->name); ?></a>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

                </div>
                <h4>Pełny opis</h4>
                <p><?php echo e($video->description); ?></p>
                <span class="upper-label">Dodał</span>
                <span class="video-author">Strefa Kursów</span>

                
  <div class="edit-button">
                    <a href="<?php echo e(action('VideosController@edit', $video->id)); ?>" "class="btn btn-primary btn-lg">
                        Edytuj Video
                    </a>
<div class="edit-button">
                    <a href="<?php echo e(action('VideosController@dest', $video->id)); ?>"  class="btn btn-primary btn-lg">
                        Kasuj Video
                    </a>

                </div>
                             <form action="<?php echo e(action('VideosController@dest', $video->id)); ?>" method="get" >
   
  

  <div class="form-group">
  <button class="btn btn-primary">Kasuj</button>
  </div>
</form>
            </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>