<?php $__env->startSection('content'); ?>

<div class="videos-header card">
    <h2>Najnowsze filmy</h2>
</div>
<div class="row">
  <?php $__currentLoopData = $videos1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vid): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <!-- Single video -->
		    <div class="col-xs-12 col-md-6 col-lg-4 single-video">
		        <div class="card">
		        
		            <div class="embed-responsive embed-responsive-16by9">
		                <iframe class="embed-responsive-item" src="<?php echo e($vid->url); ?>?showinfo=0" frameborder="0" allowfullscreen></iframe>
		            </div>
		            <div class="card-content">
		           
		            

		                       <a href="<?php echo e(action('VideosController@show', $vid->id)); ?>" "class="btn btn-primary btn-lg">
		                    <h4><?php echo e($vid->title); ?></h4>
		                </a>
		           
		                <p><?php echo e($vid->description); ?></p>
		                <span class="upper-label">Dodał</span>
		                <span class="video-author"><?php echo e($vid->user->name); ?></span>
		                
		                 <span class="upper-label">Kategoria</span>
		                <?php $__currentLoopData = $vid->categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		                
		                 <span class="video-author"><?php echo e($category->name); ?></span>
		                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

		                  <img src='<?php echo e(asset('avatars')."/" .$vid->user_photo); ?>'>

		            </div>
		            
		        </div>
		    </div>

 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>